﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoenishTheGame
{
    class FinishLine
    {
        int _location;
        List<Word> _words;

        public event
            Action<Word> WordCrossLineEvent;

        public FinishLine(int x, List<Word> words)
        {
            _location = x;
            _words = words;
        }

        public void Update()
        {
            for (int i = 0; i < Console.WindowHeight; i++)
            {
                Console.SetCursorPosition(_location, i);
                Console.Write("|");
            }

            for (int i = 0; i < _words.Count; i++)
            {
                if (_words[i].GetLocation().x >= _location)
                {
                    WordCrossLineEvent(_words[i]);
                }
            }
        }

        public int GetLocation()
        {
            return _location;
        }
    }
}
