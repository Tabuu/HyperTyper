﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoenishTheGame
{
    class Word
    {
        string _word;
        float _speed;
        Vector2 _location;
        float _timer = 0;
        ConsoleColor _color;
        public Word(string word, Vector2 location, ConsoleColor color)
        {
            _word = word;
            _location = location;
            _color = color;
        }

        public void SetSpeed(float speed)
        {
            _speed = speed;
        }

        public void Update()
        {
            Console.ForegroundColor = _color;
            _timer += 0.05f;
            if (_timer >= _speed)
            {
                _timer = 0;
                _location.x += 1;
            }

            Console.SetCursorPosition(_location.x - 1, _location.y);
            Console.Write(" ");

            Console.SetCursorPosition(_location.x, _location.y);
            Console.Write(_word);

            Console.ResetColor();
        }

        public void Delete()
        {
            Console.SetCursorPosition(_location.x, _location.y);
            for (int i = 0; i < _word.Length; i++)
            {
                Console.Write(" ");
            }
        }

        public Vector2 GetLocation()
        {
            return _location;
        }

        public string GetWord()
        {
            return _word;
        }
    }
}
