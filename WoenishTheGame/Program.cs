﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoenishTheGame
{
    class Program
    {
        static bool _RUNNING = true;
        static Stopwatch _stopwatch = new Stopwatch();
        static void Main(string[] args)
        {
            Game game = new Game();

            game.Start();
            _stopwatch.Start();
            while (_RUNNING)
            {
                if (_stopwatch.ElapsedMilliseconds >= 50)
                {
                    game.Update();
                    _stopwatch.Restart();
                }
            }
        }
    }
}
